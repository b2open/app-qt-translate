#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "b2translate.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    B2I18n i18n;
    QQmlApplicationEngine engine;

    QObject::connect(&i18n, &B2I18n::translateRequest, &engine, &QQmlApplicationEngine::retranslate);

    engine.rootContext()->setContextProperty("i18n", &i18n);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
