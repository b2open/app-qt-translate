import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Window 2.10

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Olá Mundo")


    SwipeView {
        id: view
        currentIndex: 0
        anchors.fill: parent

        Item {
            id: page1

            Rectangle {
                anchors.fill: parent
                color: "gold"

                Text {
                    id: titleMain
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 48
                    font.weight: Font.ExtraBold
                    color: "#181818"
                    text: qsTr("APLICAÇÃO\nDEMO")
                }
            }

        }
        Item {
            id: page2

            Rectangle {
                anchors.fill: parent
                color: "lime"

                Text {
                    id: l1
                    width: parent.width
                    height: parent.height
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 38
                    font.weight: Font.DemiBold
                    color: "#223e32"
                    text: qsTr("AGUARDANDO COMUNICAÇÃO")
                    wrapMode: Text.WordWrap
                }
            }

        }
        Item {
            id: page3

            Rectangle {
                anchors.fill: parent
                color: "#424242"

                Text {
                    id: langTitle
                    width: parent.width
                    height: parent.height/4
                    anchors.top: parent.top
                    anchors.topMargin: 5
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 24
                    font.weight: Font.DemiBold
                    color: "lawngreen"
                    text: qsTr("TRADUÇÃO")
                }

                Image {
                    id: langEnglish
                    width: 128
                    height: 128
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.horizontalCenter
                    anchors.leftMargin: 15
                    sourceSize: Qt.size(width,height)
                    source: "qrc:///RES/ICONS/ingles.svg"

                    MouseArea {
                        anchors.fill: parent
                        onPressed: { langEnglish.scale = 0.95; }
                        onReleased: {
                            langEnglish.scale = 1.0;

                            // Chamada para Translate -> English
                            i18n.translate("en");
                        }
                    }
                }

                Image {
                    id: langPortuguese
                    width: 128
                    height: 128
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.horizontalCenter
                    anchors.rightMargin: 15
                    sourceSize: Qt.size(width,height)
                    source: "qrc:///RES/ICONS/portugues.svg"

                    MouseArea {
                        anchors.fill: parent
                        onPressed: { langPortuguese.scale = 0.95; }
                        onReleased: {
                            langPortuguese.scale = 1.0;


                            // Chamada para Translate -> Portugues
                            i18n.translate("ptbr");
                        }
                    }
                }

            }

        }
    }

    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
