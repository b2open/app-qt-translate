TARGET = b2qti18n
QT += quick
CONFIG += c++11

SOURCES += \
        main.cpp

RESOURCES += qml.qrc

TRANSLATIONS += i18n/b2qti18n_ptbr.ts i18n/b2qti18n_en.ts

unix:target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    b2translate.h



# Executa a rotina para criar o .qm da traducao
qtPrepareTool(LRELEASE, lrelease)
for(tsfile, TRANSLATIONS) {
    qmfile = $$shadowed($$tsfile)
    qmfile ~= s,.ts$,.qm,
    qmdir = $$dirname(qmfile)
    !exists($$qmdir) {
        mkpath($$qmdir)|error("Aborting.")
    }
    command = $$LRELEASE -removeidentical $$tsfile -qm $$qmfile
    system($$command)|error("Failed to run: $$command")
    TRANSLATIONS_FILES += $$qmfile
}
