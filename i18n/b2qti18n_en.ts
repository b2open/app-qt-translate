<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="9"/>
        <source>Olá Mundo</source>
        <translation>Hello World</translation>
    </message>
    <message>
        <location filename="../main.qml" line="32"/>
        <source>APLICAÇÃO
DEMO</source>
        <translation>DEMO
APPLICATION</translation>
    </message>
    <message>
        <location filename="../main.qml" line="55"/>
        <source>AGUARDANDO COMUNICAÇÃO</source>
        <translation>AWAIT COMMUNICATION</translation>
    </message>
    <message>
        <location filename="../main.qml" line="78"/>
        <source>TRADUÇÃO</source>
        <translation>TRANSLATE</translation>
    </message>
</context>
</TS>
