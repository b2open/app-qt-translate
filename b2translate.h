#ifndef B2TRANSLATE_H
#define B2TRANSLATE_H

/*
 *  Reference: https://github.com/jaredtao/I18n
 */

#include <QCoreApplication>
#include <QDebug>
#include <QMap>
#include <QObject>
#include <QString>
#include <QTranslator>

class B2I18n: public QObject{
    Q_OBJECT

public:

    B2I18n(QObject *parent = nullptr) : QObject(parent) {
        QTranslator *en = new QTranslator(this);
        QTranslator *ptbr = new QTranslator(this);

        bool ret = en->load(QStringLiteral("i18n/%1_en.qm").arg(QCoreApplication::applicationName()));
        qInfo() << "["<<ret<<"] Carregando traducao ingles";
        ret = ptbr->load(QStringLiteral("i18n/%1_ptbr.qm").arg(QCoreApplication::applicationName()));
        qInfo() << "["<<ret<<"] Carregando traducao portugues";

        mTrans["en"] = en;
        mTrans["ptbr"] = ptbr;
    }

    Q_INVOKABLE void translate(const QString &lan) {
        if (mTrans.contains(lan)) {
            if (!mLastLan.isEmpty()) {
                QCoreApplication::removeTranslator(mTrans[mLastLan]);
            }
            mLastLan = lan;
            QCoreApplication::installTranslator(mTrans[mLastLan]);
            emit translateRequest();
            qWarning() << "translate - " << lan;
        }
    }

signals:
    void translateRequest();

private:
    QMap<QString, QTranslator *> mTrans;
    QString mLastLan;
};

#endif // B2TRANSLATE_H
