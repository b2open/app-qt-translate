App-Qt-Translate
----------------

Para gerar o **.ts** que ira processar a sintaxe `qsTr()` em QML e `tr()` em C++. Neste exemplo foi utilizado o **Qt 5.10** instalado em /opt em um computador Linux.


```sh
b2qti18n $ /opt/Qt/5.10.0/gcc_64/bin/lupdate -pro b2qti18n.pro
Info: creating stash file /home/cbueno/Projetos/B2Open/Qt/b2qti18n/.qmake.stash
  Updating 'i18n/b2qti18n_ptbr.ts'...
  Found 4 source text(s) (4 new and 0 already existing)
  Updating 'i18n/b2qti18n_en.ts'...
  Found 4 source text(s) (4 new and 0 already existing)
```


No diretorio do projeto existe o i18n, onde serão gerado os arquivos **.ts** via `lupdate`, ao compilar o projeto é acessado este diretorio e gerado o **.qm** o diretorio do destino do binario, esta instrução esta no **.pro**.


Referências
----------
https://doc.qt.io/qt-5/qtquick-internationalization.html
https://doc.qt.io/qt-5/i18n-source-translation.html

